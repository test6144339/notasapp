// index.js (o tu archivo principal)
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const app = express();
const port = process.env.PORT || 3000;

require('dotenv').config();
const noteRoutes = require('./routes/note.js');

app.use(cors());
app.use(express.json());
app.use('/api', noteRoutes);

mongoose
	.connect(process.env.MONGODB_URI, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
	})
	.then(() => console.log('Conectado a la base de datos Atlas'))
	.catch((error) => console.error(error));

app.listen(port, () => {
	console.log(`Servidor escuchando en el puerto ${port}`);
});
