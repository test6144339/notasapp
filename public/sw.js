//sw.js
importScripts('https://cdn.jsdelivr.net/npm/pouchdb@7.0.0/dist/pouchdb.min.js');
//archivo de config
importScripts('js/sw-utils.js');
importScripts('js/sw-db.js');
importScripts('firebase-messaging-sw.js');

// Crear las variables de cache
const CACHE_DYNAMIC = 'dynamic-v1'; // Para los archivos que se van a descargar
const CACHE_STATIC = 'static-v1'; // App shell
const CACHE_INMUTABLE = 'inmutable-v1'; // CDN de terceros, LIBRERIAS

const limpiarCache = (cacheName, numberItem) => {
	caches.open(cacheName).then((cache) => {
		cache.keys().then((keys) => {
			if (keys.length > numberItem) {
				cache.delete(keys[0]).then(limpiarCache(cacheName, numberItem));
			}
		});
	});
};

self.addEventListener('install', (event) => {
	const cachePromise = caches.open(CACHE_STATIC).then(function (cache) {
		return cache.addAll([
			'/',
			'/index.html',
			'/js/app.js',
			'/js/sw-utils.js',
			'/sw.js',
			'/favicon.ico',
			'static/js/bundle.js',
			// 'manifest.json',
			'/manifest.json',
		]);
	});
	const cacheInmutable = caches.open(CACHE_INMUTABLE).then(function (cache) {
		return cache.addAll([
			'https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css',
			'https://cdn.jsdelivr.net/npm/pouchdb@7.0.0/dist/pouchdb.min.js',
			'https://www.gstatic.com/firebasejs/10.6.0/firebase-messaging-compat.js',
			'https://www.gstatic.com/firebasejs/10.6.0/firebase-app-compat.js',
		]);
	});
	event.waitUntil(cachePromise, cacheInmutable);
});
// Funcion para eliminar cache anterior
self.addEventListener('activate', function (event) {
	const respuesta = caches.keys().then((keys) => {
		keys.forEach((key) => {
			if (key !== CACHE_STATIC && key.includes('static')) {
				return caches.delete(key);
			}
		});
	});
	event.waitUntil(respuesta);
});

//Estrategia de cache

self.addEventListener('fetch', (event) => {
	let respuesta;
	if (event.request.url.includes('api')) {
		//Tenemos una respuesta
		respuesta = manejoApi(CACHE_DYNAMIC, event.request);
	} else {
		respuesta = caches.match(event.request).then((res) => {
			if (res) {
				actualizaCacheStatico(CACHE_STATIC, event.request, CACHE_INMUTABLE);
				return res;
			} else {
				return fetch(event.request).then((newRes) => {
					return actualizaCacheDinamico(
						CACHE_DYNAMIC,
						event.request,
						newRes
					);
				});
			}
		});
	}
	event.respondWith(respuesta);
});

//tareas async

self.addEventListener('sync', (event) => {
	console.log('SW: Sync');
	if (event.tag === 'nuevo-post') {
		const respuesta = postearMensajes();
		event.waitUntil(respuesta);
	}
});

console.log('PouchDB version:', PouchDB.version);
