// Import the functions you need from the SDKs you need
importScripts(
	'https://www.gstatic.com/firebasejs/10.6.0/firebase-app-compat.js'
);
importScripts(
	'https://www.gstatic.com/firebasejs/10.6.0/firebase-messaging-compat.js'
);
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
	apiKey: "AIzaSyCPSenyEs0fqSQBbc-S5hwBw03L_zioajU",
	authDomain: "push-te-82243.firebaseapp.com",
	projectId: "push-te-82243",
	storageBucket: "push-te-82243.appspot.com",
	messagingSenderId: "870784263460",
	appId: "1:870784263460:web:0252dd3d8fa598544b7fb1",
	measurementId: "G-VD374TZC8V"
  };

// Initialize Firebase
const app = firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging(app);

messaging.onBackgroundMessage((payload) => {
	console.log('Recibiendo mensaje en segundo plano');
	const tituloNotificacion = payload.notification.title;
	const options = {
		body: payload.notification.body,
		icon: './favicon.ico',
	};
	self.registration.showNotification(tituloNotificacion, options);
});
