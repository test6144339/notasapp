import React, { useState, useEffect } from 'react';
//import { v4 as uuid } from 'uuid';
//import localStorage from 'local-storage';
import '../styles/App.css';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
//import DetalleTarea from './components/DetalleTarea.js';
import InputBuscador from '../components/InputBuscador';
//import MensajeNoEncontrado from './components/MensajeNoEncontrado';
import PendingTaskCounter from '../components/PendingTaskCounter';
//import CompletedTaskCounter from './components/CompletedTaskCounter';
import axios from 'axios';
import { getToken, onMessage } from 'firebase/messaging';
import { messaging } from '../firebase';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
//import { Form } from 'antd';

function NoteForm() {
	const getTokenNotification = async () => {
		const token = await getToken(messaging, {
			vapidKey:
				'BD38eIyWEs9_HwwWwOEdm9aNf4YhzwFAQS-_qfL5Q5RnNKhaez63Aq7wzbb312xQ3Sf7b9edlvBV6lSa2Mtby7E',
		}).catch((err) => console.log('no se pudo obtener el token', err));

		if (token) {
			console.log('Token: ', token);
		}
		if (!token) {
			console.log('No hay token disponible');
		}
	};
	const notificarme = () => {
		if (!window.Notification) {
			console.log('Este navegador no soporta notificaciones');
			return;
		}
		if (Notification.permission === 'granted') {
			getTokenNotification();
		} else if (
			Notification.permission !== 'denied' ||
			Notification.permission === 'default'
		) {
			Notification.requestPermission((permission) => {
				if (permission === 'granted') {
					getTokenNotification();
				}
			});
		}
	};

	notificarme();

	const [tasks, setTasks] = useState([]);
	const [newTask, setNewTask] = useState({
		_id: null,
		title: '',
		description: '',
		dueDate: null,
		priority: 'Normal',
		estado: 'pendiente', // Establece el estado como "pendiente" al agregar una nueva tarea
	});
	const [pendingTaskCount, setPendingTaskCount] = useState(0);

	useEffect(() => {
		const fetchData = async () => {
			try {
				const response = await axios.get('http://localhost:3000/api/note');
				const tasksFromAPI = response.data;
				setTasks(tasksFromAPI);

				const pendingTasks = tasksFromAPI.filter(
					(task) => task.estado === 'pendiente'
				);
				setPendingTaskCount(pendingTasks.length);
			} catch (error) {
				console.error('Error al obtener las tareas desde la API:', error);
			}
		};

		const initializeMessaging = async () => {
			await getTokenNotification();

			onMessage(messaging, (message) => {
				console.log('onMessage: ', message);
				toast(message.notification.title);
			});
		};

		fetchData();
		initializeMessaging();
	}, [pendingTaskCount]); // Asegúrate de pasar un array vacío como dependencia para que se ejecute solo una vez

	const agregarTarea = async (e) => {
		e.preventDefault();

		if (newTask.title.trim() === '') return;

		try {
			const response = await axios.post(
				'http://localhost:3000/api/note',
				newTask
			);

			const savedTask = response.data;

			if (savedTask) {
				// Solo si la inserción fue exitosa, actualiza localmente la lista de tareas
				setTasks((prevTasks) => [...prevTasks, savedTask]);
				setNewTask({
					_id: null,
					title: '',
					description: '',
					dueDate: null,
					priority: 'Normal',
					estado: 'pendiente',
				});

				// Espera un breve período antes de realizar la solicitud GET
				setTimeout(() => {
					// Ahora realiza la solicitud GET para obtener la lista actualizada de tareas
					const fetchData = async () => {
						try {
							const response = await axios.get(
								'http://localhost:3000/api/note'
							);
							const tasksFromAPI = response.data;
							setTasks(tasksFromAPI);

							// Calcula el contador de tareas pendientes
							const pendingTasks = tasksFromAPI.filter(
								(task) => task.estado === 'pendiente'
							);
							setPendingTaskCount(pendingTasks.length);
						} catch (error) {
							console.error(
								'Error al obtener las tareas desde la API:',
								error
							);
						}
					};

					fetchData();
				}, 500); // Espera 500 milisegundos (ajusta según sea necesario)
			} else {
				// Manejar el caso donde la inserción no fue exitosa, mostrar un mensaje al usuario, etc.
			}
		} catch (error) {
			console.error(error);
			// Manejar el error, posiblemente mostrar un mensaje al usuario
		}
	};

	const eliminarTarea = async (taskId) => {
		if (!taskId || typeof taskId !== 'string') {
			console.error('ID de tarea no válido:', taskId);
			return;
		}

		try {
			await axios.delete(`http://localhost:3000/api/note/${taskId}`);
			// Calcula el nuevo contador de tareas pendientes después de eliminar
			setTasks((prevTasks) => {
				const updatedTasks = prevTasks.filter(
					(task) => task._id !== taskId
				);
				const pendingTasks = updatedTasks.filter(
					(task) => task.estado === 'pendiente'
				);
				setPendingTaskCount(pendingTasks.length);
				return updatedTasks;
			});
		} catch (error) {
			console.error('Error al eliminar la tarea:', error);
			// Manejar el error, posiblemente mostrar un mensaje al usuario
			return;
		}
	};

	return (
		<div className='ContainerPrincipal'>
			<ToastContainer />
			<h1 style={{ textAlign: 'center' }}>Mi Lista de Tareas</h1>
			<form onSubmit={agregarTarea} className='CuadroAgregar'>
				<input
					className='inputTitutoTarea'
					type='text'
					placeholder='Título de la tarea'
					value={newTask.title}
					onChange={(e) =>
						setNewTask({ ...newTask, title: e.target.value })
					}
				/>
				<textarea
					className='inputDescTarea'
					type='text'
					placeholder='Descripción de la tarea'
					value={newTask.description}
					onChange={(e) =>
						setNewTask({ ...newTask, description: e.target.value })
					}
				/>
				<div
					style={{
						display: 'flex',
						flexDirection: 'row',
						alignItems: 'flex-start',
						justifyContent: 'space-between',
					}}
				>
					<div
						style={{
							display: 'flex',
							flexDirection: 'column',
							marginRight: '40px',
						}}
						className='datePickerContainer'
					>
						<label className='datePickerLabel'>
							Fecha de vencimiento:
						</label>
						<DatePicker
							selected={newTask.dueDate}
							onChange={(date) =>
								setNewTask({ ...newTask, dueDate: date })
							}
							showTimeSelect
							timeFormat='HH:mm'
							timeIntervals={15}
							timeCaption='Hora'
							dateFormat='MMMM d, yyyy h:mm aa'
							placeholderText='Selecciona una fecha y hora'
						/>
					</div>
					<div
						style={{
							display: 'flex',
							flexDirection: 'column',
							marginRight: '40px',
						}}
					>
						<label className='datePickerLabel'>Prioridad:</label>
						<select
							className='inputTituloTarea'
							value={newTask.priority}
							onChange={(e) =>
								setNewTask({ ...newTask, priority: e.target.value })
							}
						>
							<option value='Urgente'>Urgente</option>
							<option value='Normal'>Normal</option>
							<option value='Poco Urgente'>Poco Urgente</option>
						</select>
					</div>
					<button
						className='btnAgregarTarea'
						type='submit'
						style={{ marginTop: '15px' }}
					>
						Agregar Tarea
					</button>
				</div>
			</form>
			<InputBuscador />
			<PendingTaskCounter count={pendingTaskCount} />
			<ul className='ulListaTareas'>
				{tasks.map((task, index) => (
					<li
						className={`tareaListada ${
							task.estado === 'pendiente' ? 'pendiente' : 'terminado'
						}`}
						key={task._id || index}
					>
						<strong>Título:</strong> {task.title} <br />
						<strong>Descripción:</strong> {task.description} <br />
						<button
							className='btnEliminarTarea'
							onClick={() => eliminarTarea(task._id)}
						>
							Eliminar
						</button>
					</li>
				))}
			</ul>
		</div>
	);
}

export default NoteForm;
