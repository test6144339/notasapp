import React, { useEffect } from 'react';
import { getAuth, signInAnonymously } from 'firebase/auth';
import { Link } from 'react-router-dom';
import { Button } from 'antd';
import { getMessaging, getToken } from 'firebase/messaging';
import img from '../assets/notepad.png';

const styles = {
	container: {
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'center',
		height: '100vh', // Ajusta la altura según tus necesidades
	},
	img: {
		maxWidth: 600, // Ajusta el tamaño de la imagen según tus necesidades
	},
};

const Login = () => {
	useEffect(() => {
		requestNotificationPermission();
	}, []);

	const requestNotificationPermission = async () => {
		try {
			const messaging = getMessaging();
			const permission = await Notification.requestPermission();

			if (permission === 'granted') {
				console.log('Permiso de notificación otorgado');
				const token = await getToken(messaging);
				console.log('Token de notificación:', token);
			} else {
				console.warn('El usuario denegó el permiso de notificación');
			}
		} catch (error) {
			console.error('Error al solicitar permiso de notificación:', error);
		}
	};

	const login = async () => {
		signInAnonymously(getAuth()).then((usuario) => console.log(usuario));
	};

	return (
		<div style={styles.container}>
			<img style={styles.img} src={img} alt='img' />
			<Button type='primary' onClick={login}>
				<Link to='/notes'>Iniciar Sesión</Link>
			</Button>
		</div>
	);
};

export default Login;
